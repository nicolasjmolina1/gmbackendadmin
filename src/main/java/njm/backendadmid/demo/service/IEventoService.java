package njm.backendadmid.demo.service;

import njm.backendadmid.demo.domain.NotFoundException;
import njm.backendadmid.demo.domain.entity.Evento;
import org.hibernate.service.spi.ServiceException;

import javax.sql.rowset.serial.SerialException;
import java.util.Date;
import java.util.List;

public interface IEventoService {

    List<Evento> getEventosList() throws Exception, NotFoundException;
    Evento updateEvento(Evento eventoModificado) throws SerialException;
    Evento getEventoById(Long idEvento) throws NotFoundException;
    Evento createEvento(Evento eventoNuevo) throws ServiceException;
    Boolean deleteEventoById(Long id) throws NotFoundException;
    List<Evento> getEventosListFiltrados(Date inicio, Date fin) throws NotFoundException;
}
