package njm.backendadmid.demo.service;

import njm.backendadmid.demo.domain.NotFoundException;
import njm.backendadmid.demo.domain.entity.Evento;
import njm.backendadmid.demo.repository.IEventoRepository;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EventoService implements IEventoService{
    List<Evento> listaEventos = new ArrayList<>();

    @Autowired
    private IEventoRepository iEventoRepository;

    @Override
    public List<Evento> getEventosList() throws NotFoundException {
        try{
            listaEventos = iEventoRepository.findAll();
        }catch (Exception e){
            throw new NotFoundException("Error en el servicio de busqueda");
        }
        return listaEventos;
    }

    @Override
    public Evento createEvento(Evento eventoNuevo) throws ServiceException{
        Evento evento;
        try{
            eventoNuevo.setFoto("../../img/back_03.jpg");
            evento = iEventoRepository.save(eventoNuevo);
        }catch (ServiceException e){
            throw new ServiceException("Error con el servicio de guardar evento");
        }
        return evento;
    }

    @Override
    public Evento getEventoById(Long idEvento) throws NotFoundException {
        Optional<Evento> eventoOptional;
        try{
            eventoOptional = iEventoRepository.findById(idEvento);
            if(eventoOptional.isPresent()){
                return eventoOptional.get();
            }else{
                throw new NotFoundException("No hay resultados asignados");
            }
        }catch (Exception e) {
            throw new ServiceException("Error en el servicio de busqueda");
        }
    }

    @Override
    public Boolean deleteEventoById(Long id) throws NotFoundException {
        Boolean response = false;
        try{
            iEventoRepository.deleteById(id);
            response = true;
        }catch (Exception e){
            throw new NotFoundException("Error con el servicio de eliminar evento");
        }
        return response;
    }

    @Override
    public List<Evento> getEventosListFiltrados(Date inicio, Date fin) throws NotFoundException {
        List<Evento> listaEventosFiltrados = null;
        try{
            listaEventosFiltrados = iEventoRepository.findEventsByDate(inicio, fin);
        }catch (Exception e){
            throw new NotFoundException("Error en el servicio de busqueda filtradas");
        }
        return listaEventosFiltrados;
    }

    @Override
    public Evento updateEvento(Evento eventoModificado) throws ServiceException{
        Evento evento;
        try{
            evento = iEventoRepository.save(eventoModificado);
        }catch (ServiceException e){
            throw new ServiceException("Error con el servicio de actualizar evento");
        }
        return evento;
    }
}
