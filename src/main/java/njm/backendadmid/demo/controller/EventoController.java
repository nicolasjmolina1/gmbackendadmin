package njm.backendadmid.demo.controller;

import njm.backendadmid.demo.domain.NotFoundException;
import njm.backendadmid.demo.domain.entity.Evento;
import njm.backendadmid.demo.service.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/evento")
@CrossOrigin
public class EventoController {

    @Autowired
    private EventoService eventoService;

    @GetMapping("/listado-eventos")
    public List<Evento> getEventosList(){
        List<Evento> listaEventos = new ArrayList<>();
        try {
            listaEventos = eventoService.getEventosList();
        } catch (NotFoundException e) {
            new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        Collections.reverse(listaEventos);
        return listaEventos;
    }

    @GetMapping("/buscar/{id}")
    public Evento getEventoById(@PathVariable Long id){
        Evento evento = null;
        try{
            evento = eventoService.getEventoById(id);
        }catch (Exception e){
            new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NotFoundException e) {
            new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return evento;
    }

    @PutMapping("/actualizar")
    public Evento updateEvento(@RequestBody Evento eventoModificado){
        Evento evento = null;
        try{
            evento = eventoService.updateEvento(eventoModificado);
        }catch (Exception e){
            new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return evento;
    }

    @PostMapping("/crear")
    @ResponseStatus(HttpStatus.CREATED)
    public Evento createEvento(@RequestBody Evento eventoNuevo){
        Evento evento = null;
        try{
            evento = eventoService.createEvento(eventoNuevo);
        }catch (Exception e){
            new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return evento;
    }

    @DeleteMapping("/eliminar/{id}")
    public Boolean deleteEvento(@PathVariable("id") Long id){
        Boolean response = false;
        try{
            eventoService.deleteEventoById(id);
            response = true;
        }catch (NotFoundException e){
            new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

    @GetMapping("/filtrar/{inicio}/{fin}")//{inicio}/{fin}
    public List<Evento> listaEventosPorFecha(@PathVariable("inicio") Date inicio, @PathVariable("fin") Date fin){//, @PathVariable("fin") String fin

        /*String[] parts = fecha.split("=");
        String inicio = parts[0];
        String fin = parts[1];*/

        /*Date dateInit = null;
        Date dateFin = null;
        try {
            dateInit= new SimpleDateFormat("dd-MM-yyyy").parse(inicio);
            dateFin = new SimpleDateFormat("dd-MM-yyyy").parse(fin);
            System.out.println(inicio+"||"+fin);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/


        List<Evento> listaEventosFiltrados = new ArrayList<>();
        try {
            listaEventosFiltrados = eventoService.getEventosListFiltrados(inicio, fin);
        }catch (Exception e){
            new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (NotFoundException e) {
            new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return listaEventosFiltrados;
    }
}
