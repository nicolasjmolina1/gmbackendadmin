package njm.backendadmid.demo.domain;

public class ServiceException extends Throwable{
    public ServiceException(String message) {
        super(message);
    }
}
