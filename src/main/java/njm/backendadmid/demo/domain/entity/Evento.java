package njm.backendadmid.demo.domain.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "eventos")
public class Evento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    @Temporal(TemporalType.DATE)
    private Date fecha;
    private String foto;
    private String descripcion;

    public Evento() {
    }

    public Evento(Long id, String title, Date fecha, String foto) {
        this.id = id;
        this.title = title;
        this.fecha = fecha;
        this.foto = foto;
    }

    public Evento(Long id, String title, Date fecha, String foto, String descripcion) {
        this.id = id;
        this.title = title;
        this.fecha = fecha;
        this.foto = foto;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
