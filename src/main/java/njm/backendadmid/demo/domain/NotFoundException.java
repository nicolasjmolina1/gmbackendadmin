package njm.backendadmid.demo.domain;

public class NotFoundException extends Throwable {
    public NotFoundException(String mensaje) {
        super(mensaje);
    }
}
