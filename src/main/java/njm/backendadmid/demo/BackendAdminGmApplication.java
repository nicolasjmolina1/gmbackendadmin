package njm.backendadmid.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendAdminGmApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendAdminGmApplication.class, args);
	}

}
