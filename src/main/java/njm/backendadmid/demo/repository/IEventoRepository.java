package njm.backendadmid.demo.repository;

import njm.backendadmid.demo.domain.entity.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface IEventoRepository extends JpaRepository<Evento, Long> {

    @Query("SELECT e FROM Evento e WHERE e.fecha BETWEEN ?1 AND ?2")
    List<Evento> findEventsByDate(Date inicio, Date fin);
}
